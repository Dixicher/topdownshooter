// Copyright Epic Games, Inc. All Rights Reserved.

#pragma once

#include "CoreMinimal.h"
#include "GameFramework/Character.h"
#include "TopDownShooter\FunctionLibrary\Types.h"
#include "TopDownShooterCharacter.generated.h"

UCLASS(Blueprintable)
class ATopDownShooterCharacter : public ACharacter
{
	GENERATED_BODY()

public:
	ATopDownShooterCharacter();

	// Called every frame.
	virtual void Tick(float DeltaSeconds) override;

	virtual void SetupPlayerInputComponent(class UInputComponent* NewInputComponent)  override;

	/** Returns TopDownCameraComponent subobject **/
	FORCEINLINE class UCameraComponent* GetTopDownCameraComponent() const { return TopDownCameraComponent; }
	/** Returns CameraBoom subobject **/
	FORCEINLINE class USpringArmComponent* GetCameraBoom() const { return CameraBoom; }
	/** Returns CursorToWorld subobject **/
	FORCEINLINE class UDecalComponent* GetCursorToWorld() { return CursorToWorld; }

private:
	/** Top down camera */
	UPROPERTY(VisibleAnywhere, BlueprintReadOnly, Category = Camera, meta = (AllowPrivateAccess = "true"))
	class UCameraComponent* TopDownCameraComponent;

	/** Camera boom positioning the camera above the character */
	UPROPERTY(VisibleAnywhere, BlueprintReadOnly, Category = Camera, meta = (AllowPrivateAccess = "true"))
	class USpringArmComponent* CameraBoom;

	/** A decal that projects to the cursor location. */
	UPROPERTY(VisibleAnywhere, BlueprintReadOnly, Category = Camera, meta = (AllowPrivateAccess = "true"))
	class UDecalComponent* CursorToWorld;

public:

	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = Movement)
	EMovementState MovementState = EMovementState::Run_State;

	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = Movement)
	FCharacterSpeed MovementSpeed;

	UFUNCTION()
	void InputAxisX(float Value);
	UFUNCTION()
	void InputAxisY(float Value);
	
	UFUNCTION()
	void InputMouseWheelAxis(float Value);

	UFUNCTION()
		void  RMB_Pressed();
	UFUNCTION()
		void  RMB_Released();
	UFUNCTION()
		void  Ctrl_Pressed();
	UFUNCTION()
		void  Ctrl_Released();
	UFUNCTION()
		void  Shift_Pressed();
	UFUNCTION()
		void  Shift_Released();


	float AxisX = 0.f;
	float AxisY = 0.f;

	float MouseWheelAxis = 0.f;

	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = MovementState)
	bool bAimStatus;
	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = MovementState)
	bool bWalkStatus;
	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = MovementState)
	bool bSprintStatus;
	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = Movement)
	bool bMovementForward;
	

	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = Camera)
	float MaxTargetArmLenght = 1500.f;
	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = Camera)
	float MinTargetArmLenght = 600.f;
	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = Camera)
	float StepZoom = 10.f;

	UFUNCTION()
	void MovementTick(float DeltaTime);

	UFUNCTION()
	void CameraSmoothZoom(float DeltaTime);

	UFUNCTION(BlueprintCallable)
	void CharacterStateUpdate(EMovementState NewMovementState);

	UFUNCTION(BlueprintCallable)
	void ChangeMovementStatus();
};

