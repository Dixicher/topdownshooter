// Copyright Epic Games, Inc. All Rights Reserved.

#include "TopDownShooterCharacter.h"
#include "UObject/ConstructorHelpers.h"
#include "Camera/CameraComponent.h"
#include "Components/DecalComponent.h"
#include "Components/CapsuleComponent.h"
#include "GameFramework/CharacterMovementComponent.h"
#include "GameFramework/PlayerController.h"
#include "GameFramework/SpringArmComponent.h"
#include "HeadMountedDisplayFunctionLibrary.h"
#include "Materials/Material.h"
#include "Kismet/GameplayStatics.h"
#include "Kismet/KismetMathLibrary.h"
#include "Engine/World.h"

ATopDownShooterCharacter::ATopDownShooterCharacter()
{
	// Set size for player capsule
	GetCapsuleComponent()->InitCapsuleSize(42.f, 96.0f);

	// Don't rotate character to camera direction
	bUseControllerRotationPitch = false;
	bUseControllerRotationYaw = false;
	bUseControllerRotationRoll = false;

	// Configure character movement
	GetCharacterMovement()->bOrientRotationToMovement = true; // Rotate character to moving direction
	GetCharacterMovement()->RotationRate = FRotator(0.f, 640.f, 0.f);
	GetCharacterMovement()->bConstrainToPlane = true;
	GetCharacterMovement()->bSnapToPlaneAtStart = true;

	// Create a camera boom...
	CameraBoom = CreateDefaultSubobject<USpringArmComponent>(TEXT("CameraBoom"));
	CameraBoom->SetupAttachment(RootComponent);
	CameraBoom->SetUsingAbsoluteRotation(true); // Don't want arm to rotate when character does
	CameraBoom->TargetArmLength = 800.f;
	CameraBoom->SetRelativeRotation(FRotator(-60.f, 0.f, 0.f));
	CameraBoom->bDoCollisionTest = false; // Don't want to pull camera in when it collides with level

	// Create a camera...
	TopDownCameraComponent = CreateDefaultSubobject<UCameraComponent>(TEXT("TopDownCamera"));
	TopDownCameraComponent->SetupAttachment(CameraBoom, USpringArmComponent::SocketName);
	TopDownCameraComponent->bUsePawnControlRotation = false; // Camera does not rotate relative to arm

	// Create a decal in the world to show the cursor's location
	CursorToWorld = CreateDefaultSubobject<UDecalComponent>("CursorToWorld");
	CursorToWorld->SetupAttachment(RootComponent);
	static ConstructorHelpers::FObjectFinder<UMaterial> DecalMaterialAsset(TEXT("Material'/Game/Blueprint/Character/M_Cursor_Decal.M_Cursor_Decal'"));
	if (DecalMaterialAsset.Succeeded())
	{
		CursorToWorld->SetDecalMaterial(DecalMaterialAsset.Object);
	}
	CursorToWorld->DecalSize = FVector(16.0f, 32.0f, 32.0f);
	CursorToWorld->SetRelativeRotation(FRotator(90.0f, 0.0f, 0.0f).Quaternion());

	// Activate ticking in order to update the cursor every frame.
	PrimaryActorTick.bCanEverTick = true;
	PrimaryActorTick.bStartWithTickEnabled = true;
}

void ATopDownShooterCharacter::Tick(float DeltaSeconds)
{
    Super::Tick(DeltaSeconds);

	if (CursorToWorld != nullptr)
	{
		if (UHeadMountedDisplayFunctionLibrary::IsHeadMountedDisplayEnabled())
		{
			if (UWorld* World = GetWorld())
			{
				FHitResult HitResult;
				FCollisionQueryParams Params(NAME_None, FCollisionQueryParams::GetUnknownStatId());
				FVector StartLocation = TopDownCameraComponent->GetComponentLocation();
				FVector EndLocation = TopDownCameraComponent->GetComponentRotation().Vector() * 2000.0f;
				Params.AddIgnoredActor(this);
				World->LineTraceSingleByChannel(HitResult, StartLocation, EndLocation, ECC_Visibility, Params);
				FQuat SurfaceRotation = HitResult.ImpactNormal.ToOrientationRotator().Quaternion();
				CursorToWorld->SetWorldLocationAndRotation(HitResult.Location, SurfaceRotation);
			}
		}
		else if (APlayerController* PC = Cast<APlayerController>(GetController()))
		{
			FHitResult TraceHitResult;
			PC->GetHitResultUnderCursor(ECC_Visibility, true, TraceHitResult);
			FVector CursorFV = TraceHitResult.ImpactNormal;
			FRotator CursorR = CursorFV.Rotation();
			CursorToWorld->SetWorldLocation(TraceHitResult.Location);
			CursorToWorld->SetWorldRotation(CursorR);
		}
	}

	MovementTick(DeltaSeconds);
	CameraSmoothZoom(DeltaSeconds);
	ChangeMovementStatus();
}

void ATopDownShooterCharacter::SetupPlayerInputComponent(UInputComponent* NewInputComponent)
{
	Super::SetupPlayerInputComponent(InputComponent);

	NewInputComponent->BindAxis(TEXT("MoveVertical"), this, &ATopDownShooterCharacter::InputAxisX);
	NewInputComponent->BindAxis(TEXT("MoveHorizontal"), this, &ATopDownShooterCharacter::InputAxisY);
	
	NewInputComponent->BindAxis(TEXT("MouseWheel"), this, &ATopDownShooterCharacter::InputMouseWheelAxis);

	NewInputComponent->BindAction(TEXT("AimStatus"), IE_Pressed, this, &ATopDownShooterCharacter::RMB_Pressed);
	NewInputComponent->BindAction(TEXT("AimStatus"), IE_Released, this, &ATopDownShooterCharacter::RMB_Released);
	NewInputComponent->BindAction(TEXT("WalkStatus"), IE_Pressed, this, &ATopDownShooterCharacter::Ctrl_Pressed);
	NewInputComponent->BindAction(TEXT("WalkStatus"), IE_Released, this, &ATopDownShooterCharacter::Ctrl_Released);
	NewInputComponent->BindAction(TEXT("SprintStatus"), IE_Pressed, this, &ATopDownShooterCharacter::Shift_Pressed);
	NewInputComponent->BindAction(TEXT("SprintStatus"), IE_Released, this, &ATopDownShooterCharacter::Shift_Released);
}

void ATopDownShooterCharacter::InputAxisX(float Value)
{
	AxisX = Value;
}

void ATopDownShooterCharacter::InputAxisY(float Value)
{
	AxisY = Value;
}

void ATopDownShooterCharacter::InputMouseWheelAxis(float Value)
{
	MouseWheelAxis = Value;
}

void ATopDownShooterCharacter::RMB_Pressed()
{
	bAimStatus = true;
	ChangeMovementStatus();
}

void ATopDownShooterCharacter::RMB_Released()
{
	bAimStatus = false;
	ChangeMovementStatus();
}

void ATopDownShooterCharacter::Ctrl_Pressed()
{
	bWalkStatus = true;
	ChangeMovementStatus();
}

void ATopDownShooterCharacter::Ctrl_Released()
{
	bWalkStatus = false;
	ChangeMovementStatus();
}

void ATopDownShooterCharacter::Shift_Pressed()
{
	bSprintStatus = true;
	ChangeMovementStatus();
}

void ATopDownShooterCharacter::Shift_Released()
{
	bSprintStatus = false;
	ChangeMovementStatus();
}

void ATopDownShooterCharacter::MovementTick(float DeltaTime)
{
	AddMovementInput(FVector(1.f, 0.f, 0.f), AxisX);
	AddMovementInput(FVector(0.f, 1.f, 0.f), AxisY);

	APlayerController* myController = UGameplayStatics::GetPlayerController(GetWorld(), 0);
	if (myController)
	{
		FHitResult HitResult;
		myController->GetHitResultUnderCursorByChannel(ETraceTypeQuery::TraceTypeQuery6, false, HitResult);

		float LookAtRotationYaw = UKismetMathLibrary::FindLookAtRotation(GetActorLocation(), HitResult.Location).Yaw;
		SetActorRotation(FQuat(FRotator(0.f, LookAtRotationYaw, 0.f)));
	}
}

void ATopDownShooterCharacter::CameraSmoothZoom(float DeltaTime)
{
	if (MouseWheelAxis > 0)
	{
		CameraBoom->TargetArmLength = UKismetMathLibrary::FInterpTo_Constant(CameraBoom->TargetArmLength, MinTargetArmLenght, DeltaTime, StepZoom * 100.f);
	}
	else if (MouseWheelAxis < 0)
	{
		CameraBoom->TargetArmLength = UKismetMathLibrary::FInterpTo_Constant(CameraBoom->TargetArmLength, MaxTargetArmLenght, DeltaTime, StepZoom * 100.f);
	}
}

void ATopDownShooterCharacter::CharacterStateUpdate(EMovementState NewMovementState)
{
	MovementState = NewMovementState;
	float ResSpeed = 600.f;

	switch (MovementState)
	{
	case EMovementState::Aim_State:
		ResSpeed = MovementSpeed.AimSpeed;
		break;
	case EMovementState::Aim_Walk_State:
		ResSpeed = MovementSpeed.AimWalkSpeed;
		break;
	case EMovementState::Walk_State:
		ResSpeed = MovementSpeed.WalkSpeed;
		break;
	case EMovementState::Run_State:
		ResSpeed = MovementSpeed.RunSpeed;
		break;
	case EMovementState::Sprint_State:
		ResSpeed = MovementSpeed.SprintSpeed;
		break;
	default:
		break;
	}

	GetCharacterMovement()->MaxWalkSpeed = ResSpeed;
}

void ATopDownShooterCharacter::ChangeMovementStatus()
{
	if (!bAimStatus && !bWalkStatus && !bSprintStatus) CharacterStateUpdate(EMovementState::Run_State);
	else if (bAimStatus && !bWalkStatus && !bSprintStatus) CharacterStateUpdate(EMovementState::Aim_State);
	else if (bAimStatus && bWalkStatus && !bSprintStatus) CharacterStateUpdate(EMovementState::Aim_Walk_State);
	else if (!bAimStatus && bWalkStatus && !bSprintStatus) CharacterStateUpdate(EMovementState::Walk_State);
	else if (!bAimStatus && !bWalkStatus && bSprintStatus && bMovementForward) CharacterStateUpdate(EMovementState::Sprint_State);
	else if (!bAimStatus && !bWalkStatus && bSprintStatus && !bMovementForward) CharacterStateUpdate(EMovementState::Run_State);
}
